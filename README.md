## Application Android de QUIZZ

Cette application gratuite permet de créer des QUIZZ et d'y répondre !

### Changelog


Toutes les modifications effectuées sur l'application sont disponibles [ici](https://gitlab.com/dorianlfv/tp_questionnaire_java/-/blob/main/CHANGELOG). 

### Créateurs 


- Kanto RANDRIANASOLO
- Xavier DELAUNAY 
- Dorian LEFEVRE

