package com.example.menu_quizz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

public class creer_quizz2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creer_quizz2);

        Intent page1 = new Intent(this,MainActivity.class);
        Button ok = (Button) findViewById(R.id.ok_button);
        Button precedent = (Button) findViewById(R.id.bouton_precedent);
        TextView q4 = (TextView) findViewById(R.id.question4);

        precedent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(page1);
                //stopService(page1);
                finish();
            }
        });

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String test = q4.getText().toString();
                Intent intent = new Intent(creer_quizz2.this,jouer_quizz1.class);
                intent.putExtra("kanto", test);
                startActivity(intent);

            }
        });}
    }
