package com.example.menu_quizz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("  QUIZZ  ");

        Button bouton_creer_quizz = (Button) findViewById(R.id.bouton_creer_quizz); // Bouton pour joindre le remplissage du quizz

        Intent creer_quizz = new Intent(MainActivity.this,creer_quizz1.class); // Intent vers le remplissage du quizz

        bouton_creer_quizz.setOnClickListener(new View.OnClickListener() { // Joindre l'activité du quizz lorsqu'on clic sur le bouton
            @Override
            public void onClick(View v) {
                startActivity(creer_quizz);
            }
        });
    }
}