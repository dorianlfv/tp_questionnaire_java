package com.example.menu_quizz;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class jouer_quizz1 extends AppCompatActivity {

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jouer_quizz1);
        setTitle("  Répondre aux questions  ");

        TextView quest1 = (TextView) findViewById(R.id.numero1);    // Ici on stocke les questions du quizz dans un attribut
        TextView quest2 = (TextView) findViewById(R.id.numero2);
        TextView quest3 = (TextView) findViewById(R.id.numero3);
        TextView reponse1 = (TextView) findViewById(R.id.repo1);    // Ici on stocke les réponses du quizz dans un attribut
        TextView reponse2 = (TextView) findViewById(R.id.repo2);
        TextView reponse3 = (TextView) findViewById(R.id.repo3);
        TextView note = (TextView) findViewById(R.id.note);     // Ici apparaitra la note
        Button valider = (Button) findViewById(R.id.button);        // Bouton pour valider ses réponses et voir sa note

        Intent intent = getIntent();    // Intent pour récupérer les données envoyé par un autre intent

        quest1.setText(intent.getStringExtra("question1")); // Ici on récupère les questions remplis précédements
        quest2.setText(intent.getStringExtra("question2"));
        quest3.setText(intent.getStringExtra("question3"));

        valider.setOnClickListener(new View.OnClickListener() { // Après avoir rempli les champs réponses, on valide avec ce bouton
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {   // Ici on vérifie si les réponses sont justes
         int n = 0;
         while (true) {
             if (!intent.getStringExtra("rep1").equals(reponse1.getText().toString())) { // on vérifie si la réponse est incorrecte
                 System.out.println("non");
             } else {   // si ce n'est pas le cas, on augmente la note de 1
                 n++;
             }

             if (!intent.getStringExtra("rep2").equals(reponse2.getText().toString())) {
                 System.out.println("non");
             } else {
                 n++;
             }

             if (!intent.getStringExtra("rep3").equals(reponse3.getText().toString())) {
                 System.out.println("non");
                 note.setText(n + "/3");
             } else {
                 n++;
             }

             note.setText(n + "/3"); // Affichage de la note

             break;
         }
             }
        });


    }
}