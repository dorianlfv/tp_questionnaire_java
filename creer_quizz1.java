package com.example.menu_quizz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class creer_quizz1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creer_quizz1);
        setTitle("  Remplir le QUIZZ  ");

        Button suivant = (Button) findViewById(R.id.suivant);   // Bouton suivant pour répondre au quizz
        Intent page2 = new Intent(this,jouer_quizz1.class);     // Intent vers la page pour répondre au quizz
        TextView q1 = (TextView) findViewById(R.id.question1);  // View question1
        TextView q2 = (TextView) findViewById(R.id.question2);  // View question2
        TextView q3= (TextView) findViewById(R.id.question3);   // View question3

        TextView r1 = (TextView) findViewById(R.id.reponse1);   // View réponse1
        TextView r2 = (TextView) findViewById(R.id.reponse2);   // View réponse2
        TextView r3= (TextView) findViewById(R.id.reponse3);    // View réponse3

        suivant.setOnClickListener(new View.OnClickListener() {     // Lorsqu'on clique sur le bouton suivant :
            @Override
            public void onClick(View v) {

                page2.putExtra("question1",q1.getText().toString());    //le putExtra pour envoyé la donnée dans une autre activité
                page2.putExtra("question2",q2.getText().toString());    // Ici on envoi les questions pour qu'ils apparaissent dans le quizz
                page2.putExtra("question3",q3.getText().toString());

                page2.putExtra("rep1",r1.getText().toString()); // Ici on envoi les réponses, pour pouvoir vérifier les réponses lors du quizz
                page2.putExtra("rep2",r2.getText().toString());
                page2.putExtra("rep3",r3.getText().toString());
                startActivity(page2);   // Intent vers la page pour répondre au quizz

            }
        });
    }
}